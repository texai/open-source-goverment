# open-source-goverment
An idea that can change the world!

Values / Principles :
- Global
- 100% open-sourced, collaborative, democratic
- Lean enthusiast
- Transparency enthusiast
  - Public Records
  - 99.9% live-streamed ? (excluding cases like national security, too much?, really?)
- Anticorrupción (asegurada mediante IT, public real-time records)
- Adaptable to current goverment systems
- Implementable desde cualquier nivel de gobierno. (a whole country, a department, etc.)
- Platform
  - Forkeable for localazed implementations (e.g. open-sourced-goverment-peru)
  - Localizaded project forkeable for specific goverments
- People can use this model from multiple authority positions
  
Ideas to debate:
  - Deveopers around the world can represent people (by commiting, voting, PRing) ?
  - Es posible garantizar públicamente el FIFO en las colas de oficinas que atiendan procesos del govierno?
  - Es posible implementar un mecanismo de voto electrónico 
  
 Call for PRs:
  - Please, help me to build this, if you really want a beter world for the next generation


If you belive that goverments systems can be improved drammatically by using technologies and an open source & lean thinking, please support us by:
  - Feel free to clon, colaborate, commit and PR.
  - Please contact-me openly and publicly vy creating an issue or making comments
  - Share this link to more peaple, speccially programmers & hackers (speccially laws hackers)
 
 